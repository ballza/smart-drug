import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(4, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

import os
import sys
sys.path.append('/home/pi/smart-drug')
import django
os.environ['DJANGO_SETTINGS_MODULE']='machine.settings'
django.setup()
from drug.models import Cointemp

try:
    cash = Cointemp.objects.get()
except:
    cash = Cointemp.objects.create()
    cash.save()

print(os.getcwd())

previous = 0
while(1):
	input = GPIO.input(4)
	if(input == 0 and previous == 1):
		cash = Cointemp.objects.get()
		print (cash.coin)
		cash.coin +=10
		cash.save()
	previous = input
