from django.db import models


class User(models.Model):
	phone_number = models.CharField(max_length=10,null=True, blank=True)
	balance = models.PositiveIntegerField(default=0)


class Drug(models.Model):
	id_tray = models.PositiveIntegerField(default=0)
	name = models.CharField(max_length=255,null=True, blank=True)
	count = models.PositiveIntegerField(default=0)
	price = models.PositiveIntegerField(default=0)
	description = models.TextField(null=True,blank=True)
	image = models.ImageField(upload_to='drug/%Y/%m', null=True, blank=True)
	is_active = models.BooleanField(default=True)

	timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
	timeupdate = models.DateTimeField(auto_now=True)
	
	class Meta:
		ordering = ['-timestamp']

	def __str__(self):
		status = " | active" if self.is_active else " | unactive"
		return self.name + status


class Order(models.Model):
	otp = models.CharField(max_length=6,null=True, blank=True)
	date = models.DateField()


class Order_list(models.Model):
	order = models.ForeignKey(Order,on_delete=models.CASCADE)
	drug = models.ForeignKey(Drug,on_delete=models.CASCADE)
	quantity = models.PositiveIntegerField(default=0)


class Tray(models.Model):
	drug = models.ForeignKey(Drug,on_delete=models.CASCADE)
	location_tray_id = models.PositiveIntegerField(default=0)
	machine_id = models.CharField(max_length=10,null=True, blank=True)

class Cointemp(models.Model):
	coin = models.PositiveIntegerField(default=0)
