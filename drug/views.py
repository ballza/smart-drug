from rest_framework import viewsets, mixins
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import detail_route, list_route
from django.conf import settings
from .models import *
from .serializers import *


class DrugViewSet(viewsets.GenericViewSet):
    queryset = Drug.objects.all()
    serializer_class = DrugSerializer

    action_serializers = {
        'fill': FillSerializer,
        'all_drug': DrugSerializer,
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()

    @list_route(methods=['get'], url_path='list')
    def all_drug(self,request):
    	drug = Drug.objects.filter(is_active=True)
    	serializer = self.get_serializer(drug, many=True)
    	return Response(serializer.data)


    @detail_route(methods=['get'], url_path='buy')
    def buy(self,request,pk):
        print(pk)
        drug = Drug.objects.filter(id_tray=pk).first()
        money = Cointemp.objects.get()
        print(drug.count)
        print(money.coin)
        if not drug or not money:
            return Response({'Drug Not Found or Money Not Found'}, status=status.HTTP_404_NOT_FOUND)
        if drug.count == 0 :
            return Response({'No Drug'},status=status.HTTP_404_NOT_FOUND)
        if money.coin == 0 :
            return Response({'No Money'},status=status.HTTP_404_NOT_FOUND)
        if drug.price > money.coin:
            return Response({'Not Enough Money'}, status=status.HTTP_404_NOT_FOUND)
        drug.count -= 1
        print(drug.count)
        # money.coin -= drug.price
        drug.save()
        # money.save()

        from pi.test_machine import buy
        import django_rq
        queue = django_rq.get_queue('default')
        queue.enqueue(buy, pk)
        # try:
        #     cash = Cointemp.objects.get()
        # except:
        #     return Response({'Success'}, status=status.HTTP_200_OK)
        # cash.coin = 0
        # cash.save()
        import requests
        import json
        session = settings.S
        url = settings.CENTRAL_HOST + "api/server/reduce-drug/"
        data = {"id_tray": pk, "count": drug.count}
        try:
            r = session.post(url, json=data)
        except:
            print("fail")
            return Response({'Sendfail'},status=status.HTTP_400_BAD_REQUEST)
        return Response({'Success'}, status=status.HTTP_200_OK)

    @list_route(methods=['post'], url_path='fill')
    def fill(self,request):
    	serializer = self.get_serializer(data=request.data)
    	if serializer.is_valid():
    		drug = Drug.objects.filter(id=serializer.data['drug_id']).first()
    		if not drug:
    			return Response({'Drug Not Found'}, status=status.HTTP_404_NOT_FOUND)
    		if drug.count + serializer.data['count'] > 10:
    			return Response({'This drug count is more than 10'}, status=status.HTTP_400_BAD_REQUEST)
    		drug.count += serializer.data['count']
    		drug.save(update_fields=['count'])

    		response = DrugSerializer(drug).data
    		return Response({'Success'}, status=status.HTTP_200_OK)
    	else:
    		return Response(status=status.HTTP_400_BAD_REQUEST)


class UserViewSet(viewsets.GenericViewSet):
    queryset = User.objects.all()
    serializer_class = DrugSerializer

    action_serializers = {
        'machine_user': CreateMachineUserSerializer,
        'top_up_user':TopUpUserSerializer,
    }


    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()


    @list_route(methods=['post'], url_path='create-machine-user')
    def machine_user(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            phone = serializer.data['phone']
            balance = serializer.data['balance']

            # user = User.objects.filter(phone_number=phone).first()
            # if user:
            #     user.balance += balance
            # else:
            #     user = User.objects.create(phone_number=phone,
            #                                balance=balance)
            # user.save()

            import requests
            import json
            session = settings.S
            url = settings.CENTRAL_HOST + "api/server/update-money/"
            data = {"phone": phone, "balance": balance}
            try:
                r = session.post(url, json=data)
                cash = Cointemp.objects.get()
            except:
                print("555+")
                return Response({'Error'},status=status.HTTP_400_BAD_REQUEST)
            cash.coin = 0
            cash.save()
            return Response({'Success'},status=status.HTTP_200_OK)
        return Response({'Error'},status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='top-up-user')
    def top_up_user(self,request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            phone = serializer.data['phone']
            cash = serializer.data['cash']
            import requests
            import json
            session = settings.S
            url = settings.CENTRAL_HOST + "api/server/topup-money/"
            data = {"phone": phone, "cash": cash}
            try:
                r = session.post(url, json=data)
                cash = Cointemp.objects.get()
            except:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            cash.coin = 0
            cash.save()
            return Response({'Success'},status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)





class OrderViewSet(viewsets.GenericViewSet):
    queryset = User.objects.all()
    serializer_class = DrugSerializer

    action_serializers = {
        'checkout_otp': OrderByOtpSerializer,
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()

    @list_route(methods=['post'], url_path='checkout_otp')
    def checkout_otp(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            otp = serializer.data['otp']
            order = Order.objects.filter(otp=otp).first()
            if not order:
                return Response({'order not found'}, status=status.HTTP_404_NOT_FOUND)

            import requests
            from pi.test import buy

            import django_rq
            queue = django_rq.get_queue('default')

            order_list = Order_list.objects.filter(order=order)
            drug_list = []
            for orders in order_list:
                drug = Drug.objects.filter(id=orders.drug.id).first()
                drug.count -= orders.quantity

                inputs = [drug.id_tray, orders.quantity]
                queue.enqueue(buy, inputs)

                orders.delete()
                drug.save(update_fields=['count'])

                drug_list.append({
                    "id_tray": drug.id_tray,
                    "count": drug.count
                })
            order.delete()
            print(drug_list)

            # send update drug to central server
            session = settings.S
            url = settings.CENTRAL_HOST + "api/server/update-drug/"
            data = {"drug_list": drug_list}
            try:
                response = session.post(url, json=data)
            except:
                return Response({"error": "update error"}, status=status.HTTP_400_BAD_REQUEST)
            if response.status_code != 201:
                return Response({"error": "update error"}, status=status.HTTP_400_BAD_REQUEST)
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class ServerViewSet(viewsets.GenericViewSet):
    queryset = User.objects.all()

    @list_route(methods=['get'], url_path='update-order')
    def send_update_data(self, request):
        import requests
        import json
        session = settings.S
        url = settings.CENTRAL_HOST + "api/server/update-order/"

        try:
            data = json.loads(session.get(url).content)
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        print(data)
        for order_input in data:
            order = Order.objects.create(otp=order_input["otp"], date=order_input["date"])
            order.save()
            for order_list in order_input["order_list"]:
                drug = Drug.objects.filter(id_tray=order_list["id_tray"]).first()
                Order_list.objects.create(order=order,
                                          quantity=order_list["quantity"],
                                          drug=drug).save()
        return Response(status=status.HTTP_200_OK)

    @list_route(methods=['get'], url_path='check-time-order')
    def check_time_order(self, request):
        from datetime import date
        import requests
        import json
        getOrder = list(Order.objects.all())
        for o in getOrder:
            if (date.today()-o.date).days >= 1:
                session = settings.S
                url = settings.CENTRAL_HOST + "api/server/delete-order/"
                data = {"otp":o.otp}
                try:
                    r = session.post(url, json=data)
                except:
                    return Response(status=status.HTTP_400_BAD_REQUEST)
                o.delete()
        return Response(status=status.HTTP_200_OK)




class CoinViewSet(viewsets.GenericViewSet):
    queryset = Cointemp.objects.all()

    action_serializers = {
        'send_update_coin': CoinSerializer,
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()

    @list_route(methods=['get'], url_path='update-coin')
    def send_update_coin(self, request):
        cash = Cointemp.objects.get()
        serializer = self.get_serializer(cash)
        return Response(serializer.data,status=status.HTTP_200_OK)

    @detail_route(methods=['get'], url_path='reduce-money')
    def reduce_money(self,request,pk):
        cash = Cointemp.objects.get()
        if not cash:
            return Response({'Server error'}, status=status.HTTP_404_NOT_FOUND)
        if cash.coin == 0 or cash.coin < int(pk):
            return Response({'You have not recharged or your money is not enough'}, status=status.HTTP_404_NOT_FOUND)
        cash.coin -= int(pk)
        cash.save()
        return Response({'Success'}, status=status.HTTP_200_OK)
