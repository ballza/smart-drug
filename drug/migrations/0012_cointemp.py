# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-03-28 22:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drug', '0011_auto_20180313_0021'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cointemp',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('coin', models.PositiveIntegerField(default=0)),
            ],
        ),
    ]
