# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-01-20 17:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drug', '0005_drug_is_active'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='otp',
            field=models.CharField(blank=True, max_length=4, null=True),
        ),
    ]
