from rest_framework import serializers
from .models import *


class DrugSerializer(serializers.ModelSerializer):

	class Meta:
		model = Drug
		fields = ('id','name', 'id_tray','count','price','description','image')


class FillSerializer(serializers.Serializer):

	drug_id = serializers.IntegerField(required=True)
	count = serializers.IntegerField(required=True)


class CreateMachineUserSerializer(serializers.Serializer):

	phone = serializers.CharField(max_length=10, required=True)
	balance = serializers.IntegerField(required=True)

class OrderByOtpSerializer(serializers.Serializer):

	otp = serializers.CharField(max_length=6, required=True)

class CoinSerializer(serializers.ModelSerializer):

	class Meta:
		model = Cointemp
		fields = ('id','coin')

class TopUpUserSerializer(serializers.Serializer):

	phone = serializers.CharField(max_length=10, required=True)
	cash = serializers.IntegerField(required=True)