from django.contrib import admin
from .models import *


class DrugAdmin(admin.ModelAdmin):
	list_display = ('id', 'id_tray','name', 'count', 'is_active')


class UserAdmin(admin.ModelAdmin):
	list_display = ('id','phone_number', 'balance')


class OrderListAdmin(admin.ModelAdmin):
	list_display = ('id', 'get_drug', 'quantity', 'get_order')

	def get_drug(self, orderlist):
		return orderlist.drug.name
	get_drug.short_description = "Drug"

	def get_order(self, orderlist):
		return orderlist.order.id
	get_order.short_description = "Order ID"


class OrderAdmin(admin.ModelAdmin):
	list_display = ('id', 'otp', 'date')

class TrayAdmin(admin.ModelAdmin):
	list_display = ('id', 'drug', 'machine_id', 'location_tray_id')

class CointempAdmin(admin.ModelAdmin):
	list_display = ('id', 'coin')

admin.site.register(Drug, DrugAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Order_list, OrderListAdmin)
admin.site.register(Tray,TrayAdmin)
admin.site.register(Cointemp,CointempAdmin)